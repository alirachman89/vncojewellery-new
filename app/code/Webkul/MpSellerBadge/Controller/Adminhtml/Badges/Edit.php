<?php
/**
 * @category   Webkul
 * @package    Webkul_MpSellerBadge
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */ 
namespace Webkul\MpSellerBadge\Controller\Adminhtml\Badges;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Webkul\MpSellerBadge\Api\BadgeRepositoryInterface;
use Webkul\MpSellerBadge\Api\SellerbadgeRepositoryInterface;
use Magento\Framework\Registry;
use Webkul\MpSellerBadge\Model\Badge;

class Edit extends Action
{
    /**
     * badge repository
     * @var badgeRepository
     */
    protected $_badgeRepository;

    /**
     * seller badge repository
     * @var SellerbadgeRepositoryInterface
     */
    protected $_sellerBadgeRepository;

    /**
     * backend session
     * @var Session
     */
    protected $_session;

    /**
     * registry
     * @var Registry
     */
    protected $_registry;

    /**
     * object of badge model
     * @var Badge
     */
    protected $_badge;

    /**
     * @param Context                        $context
     * @param Registry                       $registry
     * @param Badge                          $badge
     * @param BadgeRepositoryInterface       $badgeRepository
     * @param SellerbadgeRepositoryInterface $sellerBadgeRepository
     * @param PageFactory                    $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Badge $badge,
        BadgeRepositoryInterface $badgeRepository,
        SellerbadgeRepositoryInterface $sellerBadgeRepository,
        PageFactory $resultPageFactory
    ) {
    
        $this->_badge = $badge;
        $this->_registry = $registry;
        $this->_session = $context->getSession();
        $this->_badgeRepository = $badgeRepository;
        $this->_sellerBadgeRepository = $sellerBadgeRepository;
        parent::__construct($context);
        $this->_resultPageFactory = $resultPageFactory;
    }
    
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $badgeModel = $this->_badge;
        if ($this->getRequest()->getParam('id')) {
            $badgeModel->load($this->getRequest()->getParam('id'));
        }
        $data = $this->_session->getFormData(true);
        if (!empty($data)) {
            $badgeModel->setData($data);
        }
        $this->_registry->register('mpsellerbadge_badges', $badgeModel);
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Badges'));
        $resultPage->getConfig()->getTitle()->prepend(
            $badgeModel->getId() ? __('Update Badge ').$badgeModel->getBadgeName() : __('New Badge')
        );
        $resultPage->addBreadcrumb(__('Manage Badges'), __('Manage Badges'));
        $resultPage->addContent(
            $resultPage->getLayout()->createBlock(
                'Webkul\MpSellerBadge\Block\Adminhtml\Badges\Edit'
            )
        );

        return $resultPage;
    }

    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_MpSellerBadge::m_badge');
    }
}
