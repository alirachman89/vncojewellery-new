<?php
namespace Emizentech\CustomSmtp\Model\Transport;

/**
 * Interceptor class for @see \Emizentech\CustomSmtp\Model\Transport
 */
class Interceptor extends \Emizentech\CustomSmtp\Model\Transport implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Mail\MessageInterface $message, \Emizentech\CustomSmtp\Helper\Data $helper)
    {
        $this->___init();
        parent::__construct($message, $helper);
    }

    /**
     * {@inheritdoc}
     */
    public function sendMessage()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'sendMessage');
        if (!$pluginInfo) {
            return parent::sendMessage();
        } else {
            return $this->___callPlugins('sendMessage', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setConnection(\Zend_Mail_Protocol_Abstract $connection)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setConnection');
        if (!$pluginInfo) {
            return parent::setConnection($connection);
        } else {
            return $this->___callPlugins('setConnection', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getConnection()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getConnection');
        if (!$pluginInfo) {
            return parent::getConnection();
        } else {
            return $this->___callPlugins('getConnection', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function _sendMail()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, '_sendMail');
        if (!$pluginInfo) {
            return parent::_sendMail();
        } else {
            return $this->___callPlugins('_sendMail', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function send(\Zend_Mail $mail)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'send');
        if (!$pluginInfo) {
            return parent::send($mail);
        } else {
            return $this->___callPlugins('send', func_get_args(), $pluginInfo);
        }
    }
}
